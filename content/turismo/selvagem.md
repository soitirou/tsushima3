+++
title = 'Centro de Conservação da Vida Selvagem de Tsushima'
date = 2023-10-04T21:19:13-03:00
draft = false
+++

Uma instalação concebida para informar os visitantes sobre a ecologia e a situação atual da vida selvagem com risco de extinção em Tsushima. Com isso, o Centro pretende aprofundar os conhecimentos dos visitantes sobre a conservação da vida selvagem. Os visitantes podem observar o  gato-leopardo de Tsushima, animal endêmico que simboliza a Ilha, através de visitas guiadas.

![Centro de Conservação da Vida Selvagem de Tsushima](/images/centrofachada.jpg)
Centro de Conservação da Vida Selvagem de Tsushima

![Interior do Centro](/images/centro.jpg)
Interior do Centro

![Espaço do gato-leopardo de Tsushima](/images/centroyamaneko.jpeg)
Espaço do gato-leopardo de Tsushima