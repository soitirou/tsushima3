+++
title = 'Santuário de Watatomi'
date = 2023-10-04T21:09:50-03:00
draft = false
+++

O santuário consagra *Hikohohodemi no Mikoto* e *Toyotamahime no Mikoto*, personagens famosos do mito de *Umisachihiko* e *Yamasachihiko*. O santuário é venerado como guardiã do mar e tem dois portões torii na Baía de Asou. Quando a maré está baixa, o primeiro *torii* pode ser alcançado a pé, mas quando a maré está alta a água do mar sobe até perto do santuário.

![Santuário de Watatomi](/images/watatomi.jpg)
Santuário de Watatomi

![Série de toriis](/images/watatomiseries.jpeg)
Série de *toriis* que levam ao Santuário de Watatomi

![Quando a maré está baixa](/images/watatomipraia.jpg)
Quando a maré está baixa