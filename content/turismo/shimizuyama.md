+++
title = 'Ruínas do Castelo Shimizuyama'
date = 2023-10-04T21:17:58-03:00
draft = false
+++

A partir do topo do Monte Shimizu, a oeste do Campo Itsuhara, as muralhas de pedra permanecem intermitentes ao longo de 500 m. Existem três recintos de forma oval e, a partir do recinto perto do topo, que é considerado *ichinomaru* (recinto principal), é possível ver até a Ilha de Iki com boas condições climáticas.

![Ruínas do Castelo Shimizuyama](/images/shimizu.jpeg)
Ruínas do Castelo Shimizuyama

![Recintos do Castelo Shimizuyama](/images/shimizumaru.webp)
Os três recintos do Castelo Shimizuyama (indicados pelas setas vermelhas, da esquerda para a direita, *ichinomaru*, *ninomaru* e *sannomaru*)

![Vista aérea do Castelo Shimizuyama](/images/shimizu3recintos.png)
Vista aérea do Castelo Shimizuyama (é possível observar as muralhas que ligam os três recintos)

![Vista aérea do ichinomaru](/images/shimizuichinomaru.png)
Vista aérea do *ichinomaru*