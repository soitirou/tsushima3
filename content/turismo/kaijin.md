+++
title = 'Santuário Kaijin'
date = 2023-10-04T21:12:23-03:00
draft = false
+++

O santuário é dedicado a Toyotamahime-no-Mikoto, a divindade guardiã do mar, e é venerado pelos pescadores da ilha. É considerado como *ichinomiya* (santuário principal) de Tsushima e tem a sua origem na conquista de *Sankan* (grande área da Península Coreana) pela Imperatriz Jingu.

![Santuário Kaijin](/images/kaijin.jpg)
Santuário Kaijin

![Torii do Santuário Kaijin](/images/kaijintorii.jpg)
Torii do Santuário Kaijin