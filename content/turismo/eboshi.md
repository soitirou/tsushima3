+++
title = 'Observatório do Monte Eboshi'
date = 2023-10-04T21:07:31-03:00
draft = false
+++

O Observatório do Monte Eboshi oferece uma vista do mar com muitas ilhas, uma das mais belas rias do Japão. Fica a cerca de cinco minutos de carro do Santuário Watatomi e é um dos pontos de visita mais espectaculares de Tsushima. Situado quase no centro de Tsushima, o observatório oferece uma magnífica vista de 360 graus da Baía de Asou. Existem escadas que ligam o estacionamento ao observatório.

![Vista da Baía de Asou a partir da plataforma de observação](/images/eboshi.jpg)
Vista da Baía de Asou a partir da plataforma de observação

![Escada que liga o estacionamento ao observatório](/images/eboshiescada.jpg)
Escada que liga o estacionamento ao observatório

![Observatório do Monte Eboshi](/images/observatorio.jpg)
Observatório do Monte Eboshi

![Vista do Observatório do Monte Eboshi](/images/emcimadoobs.jpg)
Vista do Observatório do Monte Eboshi