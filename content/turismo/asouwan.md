+++
title = "Baía de Asou"
date = 2023-10-04T21:14:01-03:00
draft = false
+++

A área da Baía de Asou, designada como Parque Nacional Iki-Tsushima, é uma das melhores rias do Japão. Os visitantes podem apreciar a paisagem a partir do Observatório Eboshidake ou a partir do mar, numa embarcação da Prefeitura de Tsushima.

![Baía de Asou](/images/asouwan.jpg)
Baía de Asou

![Embarcação da Prefeitura de Tsushima](/images/emb.jpg)
Embarcação da Prefeitura de Tsushima

![Observatório Eboshidake](/images/obseboshi.jpg)
Observatório Eboshidake