+++
title = 'Praia de Miuda'
date = 2023-10-04T21:20:47-03:00
draft = false
+++

Uma bela praia, selecionada como uma das 100 melhores do Japão. A cena que espalha é da areia branca de granulação fina, uma raridade em Tsushima. A praia é rasa e, todos os anos, após a abertura da praia, fica repleta de banhistas.

![Praia de Miuda](/images/miuda.jpeg)
Praia de Miuda