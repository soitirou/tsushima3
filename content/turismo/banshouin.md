+++
title = 'Banshouin'
date = 2023-10-04T21:18:28-03:00
draft = false
+++

Um templo fundado pelo 20º senhor feudal de Tsushima, Yoshinari Sou, para rezar pelo repouso da alma do seu pai Yoshitomo, e é uma construção histórica nacional. O salão principal foi reconstruído em 1879 (Meiji 12) quando a escala do templo foi reduzida. Existem ornamentos de templo no salão principal, os três artigos budistas (incensário, castiçal e vaso para oferendas de flores) presenteado pelo rei da Coréia e os *ihais* ("tábuas com nomes dos falecidos") das séries de shoguns Tokugawa.

![Entrada de Banshouin](/images/banshouin.jpg)
Entrada de Banshouin

![Salão principal](/images/banshouinprincipal.jpg)
Salão principal

![Interior do salão principal](/images/banshouininterior.jpg)
Interior do salão principal

![Os três artigos budistas](/images/3artigos.jpg)
Os três artigos budistas de Banshouin

![*Ihais* das séries de shoguns Tokugawa](/images/tokugawa.jpg)
*Ihais* das séries de shoguns Tokugawa

![132 degrais de pedra](/images/escadadepedra.jpg)
132 degrais de pedra

![Túmulos da família Sou](/images/tumulos.jpg)
Túmulos da família Sou