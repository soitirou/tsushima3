+++
title = 'Plataforma para observação da Coréia'
date = 2023-10-04T21:09:02-03:00
draft = false
+++

A plataforma de observação que fica no norte de Tsushima. Se as condições climatéricas forem boas, até os contornos dos edifícios da cidade de Busan podem ser vistos claramente. A área circundante é designada como monumento natural nacional: "habitat natural da vegetação *hitotsubatago*". A floração ocorre geralmente entre o final de abril e o início de maio.

![Plataforma para observação da Coréia](/images/coreia.jpg)
Plataforma para observação da Coréia

![Árvores de *hitotsubatago* floridas](/images/hitotsubatago.jpeg)
Árvores de *hitotsubatago* floridas (ao fundo, a plataforma de observação)

![Península coreana observada a partir da plataforma](/images/peninsula.jpeg)
Península coreana observada a partir da plataforma (é possível ver a base da Força de Autodefesa do Japão também)