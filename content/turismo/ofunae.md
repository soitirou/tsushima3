+++
title = 'Ofunae de Tsushima'
date = 2023-10-04T21:14:46-03:00
draft = false
+++

Ruínas do local de atracação da antiga nação de Tsushima na foz do rio Kuda. Os valiosos vestígios, que mostram parte da estrutura de armazenagem dos navios de Tsushima, terão sido construídos em 1663.

![Ofunae de Tsushima](/images/ofunae.jpeg)
Ofunae de Tsushima

![Vista aérea da Ofunae de Tsushima](/images/ofuaerea.jpg)
Vista aérea da Ofunae de Tsushima