+++
title = 'Ponte Manzeki'
date = 2023-10-04T21:20:12-03:00
draft = false
+++

Um dos poucos estreitos artificiais do Japão, escavada pela antiga marinha japonesa, em 1901, com o objetivo de permitir a passagem de navios. Da ponte de Manzeki, os visitantes podem ver os múltiplos remoinhos das correntes de maré. Está no meio do Parque Manzeki, que dispõe de estacionamento e banheiros.

![Ponte Manzeki](/images/manperto.jpeg)
Ponte Manzeki

![Ponte Manzeki vista de longe](/images/manzeki.webp)
Ponte Manzeki vista de longe