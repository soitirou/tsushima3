+++
title = 'Bukeyashiki: antigas casas dos samurais'
date = 2023-10-04T21:13:12-03:00
draft = false
+++

As altas muralhas de pedra que cercavam as residências dos samurais foram conservadas. Dizem que as muralhas altas, que chegam a ter a altura do beiral do telhado, se destinava a proteger a vida dos guerreiros e a dar um aspeto estético à cidade.

![Bukeyashiki](/images/bukeyashiki.jpg)
Bukeyashiki

![Fachada de um bukeyashiki](/images/bukefachada.jpg)
Fachada de um bukeyashiki