+++
title = 'Ruínas do Castelo Kaneda'
date = 2023-10-04T21:11:14-03:00
draft = false
+++

Foi a linha da frente da defesa nacional construída pelo Império Yamato. O Castelo Kaneda é uma fortaleza-montanha de estilo coreano, construída pelo Império Yamato em 667 (Tenchi 6) para prepar a nação diante da invasão dos povos continentais. É possível ver as muralhas do castelo com mais de 2 m de altura, os portões terrestres e para vias fluviais. Hoje, é um sítio histórico nacional especial.

![Ruínas do Castelo Kaneda](/images/kaneda.jpg)
Ruínas do Castelo Kaneda
