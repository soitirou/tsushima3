+++
title = 'Ruínas do Forte de Toyo'
date = 2023-10-04T21:15:49-03:00
draft = false
+++

O forte foi construído pelo antigo exército japonês em 1934 (Showa 9). Nunca foi utilizado e é conhecido como "forte misterioso". No local, os visitantes podem ver as casernas, as plataformas, as caves e outras instalações que conservam o aspeto da época. Tsushima foi considerado um lugar militarmente estratégico há muito tempo e desde a Guerra Sino-Japonesa até a Guerra do Pacífico foram construídos 31 fortes na Ilha. O Forte de Toyo, localizado no extremo norte de Tsushima é um deles e aproveitava um canhão de um navio de guerra que ficou inoperante.

![Onde havia o canhão principal](/images/toyocanhao.jpg)
Onde havia o canhão principal

![Cave de concreto](/images/toyocave.jpg)
Cave de concreto, as paredes e os telhados possuem 2 m de espessura

![Sala de máquinas e operações](/images/toyomaq.jpg)
Sala de máquinas e operações