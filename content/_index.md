Localizada no Estreito de Tsushima, entre o arquipêlago japonês e a península coreana, a Ilha de Tsushima é administrada pela Província de Nagasaki (Japão). Historicamente, foi o palco central das relações comerciais, políticas e militares entre o Japão e as outras nações do continente asiático. Apesar de historicamente pertencer ao Japão, a Ilha de Tsushima é mais próxima da Península Coreana do que de Kyushu, conforme imagem abaixo e hoje recebe muitos visitantes coreanos.

![Localização da Ilha de Tsushima](images/ilhajpcr.jpg)
![Mapa da Ilha de Tsushima](images/ilha.jpg)Ilha de Tsushima

Dados sobre a Ilha de Tsushima:
- Localização: Cidade de Tsushima, Província de Nagasaki, Japão
- Localização marítmica: Mar do Japão (Estreito de Tsushima/Estreito da Coreia)
- Arquipélago a que pertence: arquipélago japonês
- Área (sem inclusão das ilhas menores): 696,1 km^2
- Área (com inclusão das ilhas menores): 708,5 km^2
- Extensão da costa: 915 km
- Ponto mais alto da ilha: 648,5 m (Monte Yatate)



