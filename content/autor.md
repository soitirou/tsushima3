+++
title = 'Sobre o autor'
date = 2023-10-03T18:50:07-03:00
draft = false
+++

### Soitiro Oura

![Soitiro Oura](/images/soitiro.jpeg)

Nascido em São Paulo - SP.

Cursou a Escola Técnica Estadual de São Paulo (ETESP): curso técnico de Meio Ambiente integrado ao Ensino Médio.

"Meus antepassados eram de Tsushima. Inclusive há uma região no norte de Tsushima chamado Oura, igual ao meu sobrenome. A Era de Ouro da Ilha acabou com o desenvolvimento dos meios de transporte e comunicação, mas a Ilha é rica em natureza e resquícios históricos."