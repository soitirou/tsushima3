+++
title = '(1) Período Yayoi'
date = 2023-10-05T17:08:36-03:00
draft = false
+++

![Gi-shi-wa-jin-den](/images/gishiwajinden.webp)
"Gi-shi-wa-jin-den"

O primeiro documento em que aparece uma descrição de Tsushima é o livro de história chinês "Gi-shi-wa-jin-den", compilado no final do período Yayoi (século III). Na parte do livro que conta sobre o Japão, Tsushima é a primeira nação japonesa a aparecer. Sua descrição, embora breve, é conhecida como uma obra-prima que representa o atual Tsushima também.

Tradução da parte destacada da imagem acima, um trecho do "Gi-shi-wa-jin-den": há muitos penhascos íngremes, as montanhas são profundas e as estradas são tão estreitas como trilhos de animais. Há poucos arrozais e as pessoas comem mariscos e fazem trocas comerciais com a Coréia e o Japão.

![Santuário de Watatomi](/images/watatomiseries.jpeg)
Santuário de Watatomi

Tal como descrito em "Gi-shi-wa-jin-den", o povo marítimo de Tsushima no período Yayoi utilizava os seus conhecimentos de navegação para viajar para o Japão e continente em pequenas embarcações, fazendo uso do bom e calmo porto da Baía de Asou no centro da ilha. Mas como as águas do Estreito de Tsushima ficavam agressivas de vez em quando, as viagens eram arriscadas, o que enraizou muitas crenças relacionadas aos deuses do mar em Tsushima durante este período.

O Santuário Watatomi é dedicado a Toyotama-hime, a deusa do mar, e a lenda do Palácio do Dragão tem sido transmitida ao longo dos tempos. Se seguirmos o caminho de acesso às traseiras do santuário, encontramos uma formação rochosa considerada como o túmulo de Toyotama-hime no meio de um ar sagrado.

Os povos portuários e marítimos de Tsushima da época, protegidos pela profunda fé na Toyotama-hime, introduziram no Japão várias culturas continentais, incluindo a metalurgia e a escrita. Sem Tsushima, que desempenhou um papel importante na ligação entre o Japão e o continente, e sem a rica Baía de Asou, nem mesmo os ensinamentos do budismo teriam difundido no Japão atual.