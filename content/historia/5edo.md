+++
title = '(5) Período Edo'
date = 2023-10-05T17:07:57-03:00
draft = false
+++

![Banshouin](/images/banshouin.jpg)
Fachada de Banshouin

Sou Yoshitoshi foi um senhor que viveu durante o período mais difícil de Tsushima, entre a guerra de Toyotomi Hideyoshi com a Coréia e as negociações de paz de Tokugawa Ieyasu. Depois de unificar o país, o objetivo seguinte de Hideyoshi era dominar a grande potência da China, a dinastia Ming e, apesar das objecções de Sou Yoshitoshi e Konishi Yukinaga, foi planejada uma expedição à Península Coreana. Yoshitoshi, que recebeu ordens para liderar a expedição, terá conduzido várias negociações de paz nos bastidores, sob ordens secretas de Yukinaga, que era também seu sogro. As forças japonesas dominaram temporariamente as cidades Hanseong (Seul) e Pyongyang, mas as suas rotas de abastecimento foram cortadas pela força naval de Joseon, liderada por Yi Sun-sin, e enfrentaram a resistência dos reforços da dinastia Ming e dos soldados voluntários de Joseon. As forças japonesas foram forçadas a retirar devido à morte de Hideyoshi por doença.

As atrocidades japonesas causaram um profundo ressentimento entre o povo de Joseon e desgastaram os senhores feudais do Japão ocidental. Por outro lado, Ieyasu, que não enviou tropas para Joseon, conseguiu aumentar as suas forças e venceu a Batalha de Sekigahara, tornando-se o novo governante do Japão. Yoshitoshi e o seu sogro Yukinaga apoiaram o exército contrário a Ieyasu na Batalha de Sekigahara, e Konishi Yukinaga foi executado após a derrota e a mulher de Yoshitoshi, a filha de Yukinaga, Maria, foi obrigada a divorciar, passando a viver até o fim da sua vida em Nagasaki. Por outro lado, Yoshitoshi foi perdoado por Ieyasu, que lhe ordenou que restabelecesse as relações com Joseon, que tinham sido cortadas durante a guerra causada pelo Hideyoshi. Joseon recusou o acordo de paz, mas como era necessário defender-se contra o povo Jurchéns que estava expandindo a sua influência no norte, aceitou com a condição de que Ieyasu envie primeiro a carta de estado, entre outras condições. A primeira carta de estado foi enviada em 1607.

Depois de uma vida cheia de dificuldades, incluindo a guerra de Hideyoshi contra a Coréia, a separação da sua esposa e as negociações de paz do período pós-guerra, Sou Yoshitoshi foi reconhecido pelos seus feitos pelo shogunato Tokugawa durante o Período Edo e terminou a sua vida tumultuosa como o primeiro senhor de Tsushima em 1615.

Em memória das dificuldades do seu pai Yoshitoshi, Sou Yoshinari construiu um templo para sua família, que recebeu o nome de Banshōin, em homenagem ao nome budista de Yoshitoshi. A esposa de Yoshitoshi, Maria, apesar de cristã, está consagrada no Santuário Imamiya Wakamiya, um ramo do Santuário Hachiman em Izuhara-cho.