+++
title = 'Tsushima: uma ilha de fronteira'
date = 2023-10-05T17:17:23-03:00
draft = false
+++

Situada entre o Japão e o Continente, a Provícia de Nagasaki tem sido, desde a antiguidade, um importante ponto de tráfego marítimo e um centro de comércio e intercâmbio.

As relações com a Coréia eram particularmente importantes: a Ilha de Iki (Nagasaki) estabeleceu um reino através do comércio marítimo durante o período Yayoi e a Ilha de Tsushima obteve o  monopólio do comércio e da diplomacia com a Coréia a partir da Idade Média, prosperando como entreposto comercial e lugar de encontro dos representantes das nações.

Embora o seu papel como ponto de tráfego tenha diminuído desde então, as ruínas de antigas casas, castelos e jardins testemunham a prosperidade que hove no passado, e os vestígios do intercâmbio podem ser vistos nas técnicas de produção do shochu (bebida destilada) e massa de sobá, bem como nos costumes e eventos populares.

Estas ilhas são exemplos raros de uma zona onde se pode sentir os laços profundos entre o país e o seu povo, após passar por anos de intercâmbio, que tem continuado apesar das repetidos conflitos e reconciliações típicos de ilhas fronteiriças.

![Iki e Tsushima](/images/ikitsu.png)
Iki e Tsushima