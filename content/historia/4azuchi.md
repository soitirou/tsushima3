+++
title = '(4) Período Azuchi Momoyama'
date = 2023-10-05T17:08:05-03:00
draft = false
+++

![Castelo de Shimizuyama](/images/shimizuichinomaru.png)
Um dos recintos do Castelo de Shimizuyama

O Castelo de Shimizuyama foi construído em 1591 (Tensho 19) como lugar para abrigar o daimyo Hideyoshi Toyotomi durante a invasão japonesa da Coréia. O Castelo de Shimizuyama era um importante ponto de parada militar que ligava o Castelo de Hizen-Nagoya (cidade de Karatsu, Província de Saga), que era a principal base da invasão, ao Castelo de Katsumoto em Iki e Pusan na Península Coreana.

Ao longo do cume do Monte Shimizu, a 210 m acima do nível do mar, os três recintos chamados de Ichinomaru, Ninomaru e Sannomaru são ligados por muralhas que se estendem por cerca de 500 m de leste a oeste. Os três recintos estão ligados por muralhas de pedra. Trata-se de um castelo de montanha com muralhas totalmente em pedra. De cada um dos recintos se avista a cidade de Itsuhara e o porto de Itsuhara.

Não há registo claro de quem construiu o castelo, mas tende-se a assumir que foi o governador militar Mouri Takamasa, mas com a ajuda de três senhores feudais - o Sagara de Higo-Hitoyoshi, o Takahashi de Chikugo-Miike e o Tsukushi de Chikugo-Fukushima - e o dono de Tsushima (mais tarde senhor feudal de Tsushima), Sou Yoshitoshi.

O castelo conserva os vestígios de um castelo de montanha do período Tensho em bom estado de conservação e foi classificado como Património Histórico Nacional em 1984.

![Castelo de Shimizuyama](/images/shimizu3recintos.png)
Castelo de Shimizuyama em sua totalidade