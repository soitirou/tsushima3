+++
title = '(2) Período Asuka'
date = 2023-10-05T17:08:30-03:00
draft = false
+++

![Castelo de Kaneda](/images/kaneda.jpg)
Castelo de Kaneda

A Batalha do Rio Hakuson, em 663, trouxe tensão à Tsushima. Na altura, existiam três estados separados na península coreana - Koukuri, Shiragi e Kudara - mas Kudara, que tinha sido aliado do Japão, foi destruído pelas forças combinadas da Dinastia Tang e Shiragi, e os reforços enviados pelo Império Yamato para relevantar Kudara foram derrotados no Rio Hakuson. O Império Yamato foi forçado a retirar-se da Península Coreana e, para efeitos de defesa, foram implantadas as plataformas de defesa e defensores (*sakimori*) e foi construído um castelo. O Castelo de Kaneda (667) em Minogata, na cidade de Mitsushima, é um dos mais antigos e bem preservados castelos de montanha de estilo coreano no Japão e foi designado como Patrimônio Histórico Nacional Especial.

Atualmente, foi construído um trilho na montanha que dá acesso até o Castelo de Kaneda e, do topo da montanha, é possível ver o horizonte em direção à Península Coreana, tal como os antigos defensores o devem ter visto, tornando-o num percurso popular de trekking onde se pode sentir o romance da história.