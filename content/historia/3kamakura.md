+++
title = '(3) Período Kamakura'
date = 2023-10-05T17:08:22-03:00
draft = false
+++

![Festival Anual do Santuário de Komodahama](/images/kama.webp)
Festival Anual do Santuário de Komodahama

Em 1274, durante a Batalha de Bunei, cerca de 1000 cavaleiros de um total de 33.000 (25.000 mongóis e 8.000 coreanos da dinastia Goryeo) desembarcaram na Práia de Komoda, onde encontraram Shikoku e os seus cerca de 80 cavaleiros, que foram mortos numa feroz batalha. Shikoku, de 68 anos de idade, foi mais tarde venerado como deus militar e é homenageado no Festival Anual do Santuário de Komodahama, em novembro. No evento, os descendentes de Sou (família que dominou como senhor feudal em Tsushima) e os seus vassalos participam vestidos com armaduras, e realizam uma cerimônia de Narugen, na qual fazem o barulho com seus arcos em direção ao mar.

Diz-se que o Japão foi poupado à invasão graças ao "kamikaze" durante o Incidente de Genko, mas Tsushima e Iki sofreram danos consideráveis em toda a ilha. Como vingança pelo Incidente de Genko, os piratas japoneses começaram a saquear e a raptar ativamente as pessoas na Península Coreana e na China continental. A dinastia Goryeo foi destruída, em parte devido aos danos causados pelos piratas japoneses, e Yi Seong-gye, que se notabilizou por derrotar os piratas japoneses, fundou a dinastia Joseon.

A dinastia Joseon atacou Tsushima, que era considerada a "casa de piratas japoneses" (1419), mas a guerrilha da família Sou tornou a batalha difícil. Então, a dinastia Joseon adoptaram uma política mais amena, dando autoridade comercial às pessoas influentes de Tsushima. Assim, a relação entre Tsushima e Joseon passou de uma era de conflito entre os piratas mongóis e japoneses para uma era de comércio pacífico. Esta relação pacífica continuou a sofrer reviravoltas, incluindo a Rebelião Miura (um motim dos residentes japoneses na dinastia Joseon), até a invasão da Coréia por Hideyoshi Toyotomi.

[def]: /images/kama.webp