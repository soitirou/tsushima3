+++
title = '(6) Período Meiji'
date = 2023-10-05T17:07:49-03:00
draft = false
+++

Em 1861, no final do período Edo, o navio de guerra russo Posadnik ocupou Imozaki na Baía de Asou, centro de Tsushima. O capitão Bilyov ignorou os protestos da autoridade de Tsushima, inspecionou a zona e lançou-se num tumulto, disparando e matando um guarda em Oofunakoshi Seto.

Como os russos estavam se preparando para ficar durante muito tempo, iniciando a construção de quartéis, o Shogunato enviou o magistrado Oguri Tadamasa, mas as negociações revelaram-se difíceis. Seis meses após a ocupação, os navios de guerra russos retiraram-se de Tsushima sob a pressão dos navios de guerra britânicos.

Naquela época reinava o imperialismo. Diz-se que a Rússia, prosseguindo uma política de expansão para sul, planejava ocupar a Baía de Asou em busca de um porto que não congela, enquanto os britânicos também planejavam ocupar Tsushima se a Rússia não se retirasse.

Depois disso, a Rússia continuou a sua política de expansão para sul e, após a Guerra Sino-Japonesa e a Intervenção das Três Potências, a guerra entre o Japão e a Rússia tornou-se inevitável e Tsushima, que era um ponto militar estratégico, foi fortificada ao redor da Baía de Asou.

Na Guerra Russo-Japonesa, a frota liderada por Togo Heihachiro derrotou unilateralmente a frota russa do Báltico na Batalha do Mar do Japão, vencendo decisivamente a Guerra Russo-Japonesa. Esta batalha, que ocorreu principalmente no Mar do Japão, próximo de Tsushima, é normalmente designada fora do Japão como Batalha de Tsushima.

![Reduto de Kamisaka](/images/kamisaka.jpeg)
Reduto de Kamisaka

![Forte de Himegamiyama](/images/himegamiyama.jpg)
Forte de Himegamiyama

![Forte de Toyo](/images/toyo.jpg)
Forte de Toyo